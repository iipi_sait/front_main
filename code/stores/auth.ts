import { defineStore } from 'pinia';

interface UserPayloadInterface {
    email: string;
    password: string;
}

export const useAuthStore = defineStore('auth', {
    state: () => ({
        authenticated: false,
        loading: false,
        urlget:'',
        msgerror:'',
    }),
    actions: {
        async authenticateUser({ email, password }: UserPayloadInterface) {
            const { data, pending }: any = await useFetch(this.urlget+'auth/login', {
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                body: {
                    email,
                    password,
                },
            });

            this.loading = pending
            if (data.value) {
                const token = useCookie('token') // useCookie new hook in nuxt 3
                token.value = data?.value?.token // set token to cookie
                this.authenticated = true // set authenticated  state value to true
            }

        },
        seturl(urlget){
            this.urlget=urlget
        },
        logUserOut() {
            const token = useCookie('token'); // useCookie new hook in nuxt 3
            this.authenticated = false; // set authenticated  state value to false
            token.value = null; // clear the token cookie
        },
    },
});
