export const useDatalistStore = defineStore('datalistStore', {
    state: () => ({
        datatable:[],
        urlget:"",
        tablename:""
    }),
    actions: {
        // тут делаем запрос к БД

        async fetch() {
            const token = useCookie('token');
            const response = await $fetch(this.urlget, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + token.value
                },
            })
            this.datatable = await response.table_datas
        },
        seturl(urlget){
            this.urlget=urlget
        },
        // выводим данные
        getdata(){
            return this.datatable
        },
        // Добовляем данные в массив
        async deleterecord(keyToRemove,apiUrl,tableName){

            let indexToRemove = this.datatable.findIndex(item => item.id === keyToRemove);
            if (indexToRemove !== -1) {
                this.datatable.splice(indexToRemove, 1)
                const token = useCookie('token')
                const response = await $fetch( apiUrl+'crud/delete/'+tableName, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization": "Bearer " + token.value
                    },
                    body: {"id":keyToRemove},
                })
                if(!response.ok){
                    throw new Error(response.status)
                }
            }
        }
    }
})
