export default defineNuxtConfig({
    runtimeConfig: {
        public: {
            apiBaseUrl: 'NUXT_PUBLIC_API_BASE_URL',
        }
    },
    devtools: { enabled: true },
    css: ['~/assets/css/main.css','~/assets/css/resizemain.css'],
    buildModules: ['@nuxt/vuex',"@pinia/nuxt"],
    modules: ["@pinia/nuxt","@pinia/nuxt"]
})
